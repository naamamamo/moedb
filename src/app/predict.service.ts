import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  url = ' https://hybs62w2xb.execute-api.us-east-1.amazonaws.com/naama';
  
  predict(temperature, humidity){
    let json = {"data":
      {"temperature":temperature,
       "humidity":humidity
      }
    }

    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body; 
        
      })
    );      
  }
    constructor(private http:HttpClient) { }
}