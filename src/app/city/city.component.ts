import { PredictService } from './../predict.service';
import { Weather } from './../interfaces/weather';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { WeatherService } from '../weather.service';


export interface PeriodicElement {
  name: string;
  position: number;
  temperature:number;
  humidity:number;
}


@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})

export class CityComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name','getData','temperature','humidity','predict','result']
  city:string;
  temperature:number;
  image:string;
  country:string;
  humidity:number;
  lon:number;
  lat:number;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  result:string;
  userId:string;
  cities$

  cities:any[] = [
    {position: 1, name: 'London', temperature:25, humidity:20},
    {position: 2, name: 'Paris', temperature:15, humidity:30},
    {position: 3, name: 'Tel Aviv',temperature:20, humidity:40},
    {position: 4, name: 'Jerusalem',temperature:20, humidity:50},
    {position: 5, name: 'Berlin', temperature:20, humidity:60},
    {position: 6, name: 'Rome', temperature:25, humidity:70},
    {position: 7, name: 'Dubai', temperature:23, humidity:80},
    {position: 8, name: 'Athens', temperature:22, humidity:90}
  ];
  
  
  predict(i){
    this.PredictService.predict(this.cities[i].temperature,this.cities[i].humidity).subscribe(
      res => {console.log(res);
        if(res > 50){
          var result = 'rain';
          console.log(result)
        } else {
          var result = 'not rain';
          console.log(result)
        }
        this.cities[i].result = result      }
    );  
  }  

  public save(name:string, temperature:number, humidity:number, result:string){
        this.weatherService.saveWeather(this.userId,name,temperature,humidity,result);
   }
    

  constructor(
              public authService:AuthService,
              private PredictService:PredictService,
              private weatherService:WeatherService
              ) { }

  ngOnInit(): void {
  }
}