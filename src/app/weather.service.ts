import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "92fabd59940b6bb0897195ff7886c2b9";
  private IMP = "units=metric"

  weatherCollection:AngularFirestoreCollection;
  usersCollection:AngularFirestoreCollection = this.db.collection('users');

saveWeather(userId:string,name:string,temperature:number,humidity:number,result:string){
    const weather = {name:name,temperature:temperature,humidity:humidity,result:result}; 
    this.usersCollection.doc(userId).collection('cities').add(weather);
  }
  
public getWeather(userId){
  this.weatherCollection = this.db.collection(`users/${userId}/weather`); 
  return this.weatherCollection.snapshotChanges();
  }  
    

  constructor(private http:HttpClient,
              private db:AngularFirestore) { }

  searchWeatherData(name:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${name}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server erorr');
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name, 
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      humidity:data.main.humidity,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
 
  }
}