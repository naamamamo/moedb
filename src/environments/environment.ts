// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCOe5EoxqqjNd_DAX6Db2L8EuUKoxoRzBo",
    authDomain: "testb-b5279.firebaseapp.com",
    projectId: "testb-b5279",
    storageBucket: "testb-b5279.appspot.com",
    messagingSenderId: "255319482814",
    appId: "1:255319482814:web:4725068a3a634a090fd9ef"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
